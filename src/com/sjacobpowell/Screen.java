package com.sjacobpowell;

public class Screen extends Bitmap {

	public Screen(int width, int height) {
		super(width, height);
	}

	public void render(Game game) {
		clear();
		game.balloons.forEach(
				balloon -> draw(balloon.sprite, balloon.getXInt() - balloon.sprite.width / 2, balloon.getYInt() - balloon.sprite.height / 2));
	}
}
