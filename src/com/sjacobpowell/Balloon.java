package com.sjacobpowell;

import java.util.Random;

public class Balloon extends Entity {
	private static final double BOUNCE_SPEED = 2.5;

	public Balloon() {
		sprite  = new Bitmap(8, 8);
		Random random = new Random();
		int color = random.nextInt(0xdddddd);
		color += 0x222222;
		for(int i = 0; i < sprite.pixels.length; i++) {
			sprite.pixels[i] = color;
		}
		sprite = ArtTools.circlefy(sprite, sprite.width / 2.2);
		xSpeed = random.nextDouble() * random.nextInt() % BOUNCE_SPEED;
		ySpeed = random.nextDouble() * random.nextInt() % BOUNCE_SPEED;
		xSpeed *= random.nextInt(2) == 0 ? 1 : -1;
		ySpeed *= random.nextInt(2) == 0 ? 1 : -1;
		ID = System.nanoTime();
	}
	
	@Override
	public boolean equals(Object other) {
		if(other instanceof Balloon) {
		return ID == ((Balloon)other).ID;
		} else {
			return false;
		}
	}
	
}
