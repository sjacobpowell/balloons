package com.sjacobpowell;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

public class Game {
	public int time;
	private int width;
	private int height;
	public List<Balloon> balloons;

	public Game(int width, int height) {
		this.width = width;
		this.height = height;
		balloons = new ArrayList<Balloon>();
		IntStream.range(0, 40).forEach(i -> balloons.add(new Balloon()));
		Random random = new Random();
		balloons.forEach(balloon -> {
			balloon.x = random.nextInt(width);
			balloon.y = random.nextInt(height);
		});
	}

	public void tick(boolean[] keys) {
		balloons.forEach(balloon -> {
			balloon.move();
			if (balloon.leftEdge() < 0 || balloon.rightEdge() >= width)
				balloon.xSpeed = -balloon.xSpeed;
			if (balloon.topEdge() < 0 || balloon.bottomEdge() >= height)
				balloon.ySpeed = -balloon.ySpeed;
			balloons.forEach(otherBalloon -> {
				if (!otherBalloon.equals(balloon) && balloon.isColliding(otherBalloon)) {
					if (Math.abs(balloon.x - otherBalloon.x) < Math.abs(balloon.y - otherBalloon.y)) {
						balloon.xSpeed = -balloon.xSpeed;
					} else {
						balloon.ySpeed = -balloon.ySpeed;
					}
				}
			});
		});
		time++;
	}

}
